#!/bin/bash

set -eu

readonly wp='sudo -E -u www-data -- /app/pkg/wp --path=/app/code/ --skip-themes --skip-plugins'

mkdir -p /run/wordpress/sessions /app/data/wp-snapshots /app/data/apache

# this removes the password warning
echo -e "[client]\npassword=${CLOUDRON_MYSQL_PASSWORD}" > /run/wordpress/mysql-extra
readonly mysql="mysql --defaults-file=/run/wordpress/mysql-extra --user=${CLOUDRON_MYSQL_USERNAME} --host=${CLOUDRON_MYSQL_HOST} -P ${CLOUDRON_MYSQL_PORT} ${CLOUDRON_MYSQL_DATABASE}"

if [[ ! -f /app/data/php.ini ]]; then
    echo -e "; Add custom PHP configuration in this file\n; Settings here are merged with the package's built-in php.ini\n\n" > /app/data/php.ini
fi

[[ ! -f /app/data/apache/mpm_prefork.conf ]] && cp /app/pkg/mpm_prefork.conf /app/data/apache/mpm_prefork.conf

# Used for wp rewrite
touch /app/data/htaccess

echo "==> Changing permissions"
chown -R www-data:www-data /app/data /run/wordpress

table_count=$(mysql -NB -u"${CLOUDRON_MYSQL_USERNAME}" -p"${CLOUDRON_MYSQL_PASSWORD}" -h "${CLOUDRON_MYSQL_HOST}" -P "${CLOUDRON_MYSQL_PORT}" -e "SELECT COUNT(*) FROM information_schema.tables WHERE table_schema = '${CLOUDRON_MYSQL_DATABASE}';" "${CLOUDRON_MYSQL_DATABASE}" 2>/dev/null)

if [[ "${table_count}" == "0" ]]; then
    echo "==> Copying wp-content files on first run"
    sudo -u www-data mkdir -p /app/data/wp-content
    sudo -u www-data cp -r /app/code/wp-content-vanilla/* /app/data/wp-content/

    # note: we used to set --dbprefix="" but apparently empty table prefix is not "supported"
    echo "=> Create initial WordPress config"
    $wp config create --dbname="${CLOUDRON_MYSQL_DATABASE}" --dbuser="${CLOUDRON_MYSQL_USERNAME}" --dbpass="${CLOUDRON_MYSQL_PASSWORD}" --dbhost="${CLOUDRON_MYSQL_HOST}:${CLOUDRON_MYSQL_PORT}"

    echo "=> Install WordPress"
    # --skip-email is part of 0.23.0 https://github.com/wp-cli/wp-cli/pull/2345 and https://github.com/wp-cli/wp-cli/issues/1164
    $wp core install --skip-email --url="${CLOUDRON_APP_ORIGIN}" --title="My website" --admin_user=admin --admin_password="changeme" --admin_email="admin@cloudron.local"

    if [[ -n "${CLOUDRON_MAIL_SMTP_SERVER:-}" ]]; then
        echo "==> Install smtp plugin"
        $wp plugin install /app/pkg/smtp-mailer.zip
        $wp plugin activate smtp-mailer
    fi

    # Set default post structure to what most people want. Will not work without WP_CLI_CONFIG_PATH env var
    # allow plugins here, because otherwise it warns
    sudo -E -u www-data -- /app/pkg/wp --path=/app/code/ rewrite structure --hard '/%postname%/'

    $wp config set --raw DISABLE_WP_CRON true

    # this sets up a random cookie name
    random=$(pwgen -1s 32)
    $wp config set --raw COOKIEHASH "md5('${random}')"

    $wp config set --raw WP_DEBUG false
    $wp config set --raw WP_DEBUG_LOG false
    $wp config set --raw WP_DEBUG_DISPLAY false

    # this disables the editor from WP admin area
    $wp config set --raw DISALLOW_FILE_EDIT true
    $wp config set --raw DISALLOW_UNFILTERED_HTML true
else
    # Update db settings first. otherwise, the domain/mail changes will overwrite the original db when cloning
    echo "=> Updating db settings"
    $wp config set DB_HOST "${CLOUDRON_MYSQL_HOST}:${CLOUDRON_MYSQL_PORT}"
    $wp config set DB_NAME "${CLOUDRON_MYSQL_DATABASE}"
    $wp config set DB_USER "${CLOUDRON_MYSQL_USERNAME}"
    $wp config set DB_PASSWORD "${CLOUDRON_MYSQL_PASSWORD}"

    # Update wordpress
    echo "==> Updating wordpress database"
    $wp core update-db
fi

echo "==> Updating domain related settings"
# This keeps the values in Settings -> Site/WordPress Address read-only
$wp config set WP_HOME "${CLOUDRON_APP_ORIGIN}"
$wp config set WP_SITEURL "${CLOUDRON_APP_ORIGIN}"

# https://developer.wordpress.org/advanced-administration/wordpress/wp-config/ . themes cannot be moved
$wp config set WP_CONTENT_DIR "/app/data/wp-content"

# This is only done for keeping the db dumps more useful
$wp option update siteurl "${CLOUDRON_APP_ORIGIN}"
$wp option update home "${CLOUDRON_APP_ORIGIN}"

if [[ -n "${CLOUDRON_MAIL_SMTP_SERVER:-}" ]]; then
    # read-in any user set mail from name
    echo "==> Configuring smtp mail"

    # base64 will print multiple lines by default!
    smtp_password=$(echo -n "${CLOUDRON_MAIL_SMTP_PASSWORD}" | base64 -w 0 -)

    # preserve any existing force_from_address
    force_from_address=""
    if mail_config=$($wp --format=json option get smtp_mailer_options); then
        force_from_address=$(echo "${mail_config}" | jq -r .force_from_address) # is blank if missing from json
    fi

    mailConfig=$(cat <<EOF
    {
        "smtp_host"                 : "${CLOUDRON_MAIL_SMTP_SERVER}",
        "smtp_auth"                 : true,
        "smtp_username"             : "${CLOUDRON_MAIL_SMTP_USERNAME}",
        "smtp_password"             : "${smtp_password}",
        "type_of_encryption"        : "none",
        "smtp_port"                 : "${CLOUDRON_MAIL_SMTP_PORT}",
        "from_email"                : "${CLOUDRON_MAIL_FROM}",
        "from_name"                 : "${CLOUDRON_MAIL_FROM_DISPLAY_NAME:-WordPress}",
        "force_from_address"        : "${force_from_address}",
        "disable_ssl_verification"  : ""
    }
EOF
    )

    $wp --format=json option update smtp_mailer_options "${mailConfig}"
else
    echo "==> Skipping email configuration"
fi

if [[ -n "${CLOUDRON_OIDC_ISSUER:-}" ]]; then
    default_role=""
    if $wp plugin is-installed authldap; then
        default_role=$($wp --format=json option get authLDAPOptions | jq -r ".DefaultRole")
        echo "==> Previous default role was '${default_role}'"
        echo "==> Uninstall LDAP plugin (authldap)"
        $wp plugin deactivate --uninstall authldap
    fi

    if ! $wp plugin is-installed openid-connect-generic; then
        echo "==> Install OIDC plugin"
        $wp plugin install /app/pkg/openid-connect-generic.zip
        mv /app/data/wp-content/plugins/openid-connect-generic-* /app/data/wp-content/plugins/openid-connect-generic
        $wp plugin activate openid-connect-generic

        $wp plugin install /app/pkg/cloudron-sso.zip
        $wp plugin activate cloudron-sso
    fi

    # CLOUDRON_OIDC_PROVIDER_NAME is not supported
    if oidcConfig=$(wp --format=json option get openid_connect_generic_settings); then
        oidcConfig=$(echo $oidcConfig \
                    | jq ".login_type=\"button\"" \
                    | jq ".client_id=\"${CLOUDRON_OIDC_CLIENT_ID}\"" \
                    | jq ".client_secret=\"${CLOUDRON_OIDC_CLIENT_SECRET}\"" \
                    | jq ".scope=\"openid profile email\"" \
                    | jq ".endpoint_login=\"${CLOUDRON_OIDC_AUTH_ENDPOINT}\"" \
                    | jq ".endpoint_userinfo=\"${CLOUDRON_OIDC_PROFILE_ENDPOINT}\"" \
                    | jq ".endpoint_token=\"${CLOUDRON_OIDC_TOKEN_ENDPOINT}\"" \
                    | jq ".endpoint_end_session=\"\"" \
                    | jq ".identity_key=\"preferred_username\"" \
                    | jq ".no_sslverify=\"0\"" \
                    | jq ".alternate_redirect_uri=\"0\"" \
                    | jq ".nickname_key=\"preferred_username\"" \
                    | jq ".email_format=\"{email}\"" \
                    | jq ".displayname_format=\"{given_name} {family_name}\"" \
                    | jq ".identify_with_username=\"1\"" \
                    | jq ".token_refresh_enable=\"0\"" \
                    | jq ".link_existing_users=\"1\"" \
                    | jq ".create_if_does_not_exist=\"1\"" \
                    | jq ".redirect_user_back=\"1\"" \
                    | jq ".redirect_on_logout=\"1\"" \
            )
    else
        oidcConfig=$(cat <<EOF
{
  "login_type": "button",
  "client_id": "${CLOUDRON_OIDC_CLIENT_ID}",
  "client_secret": "${CLOUDRON_OIDC_CLIENT_SECRET}",
  "scope": "openid profile email",
  "endpoint_login": "${CLOUDRON_OIDC_AUTH_ENDPOINT}",
  "endpoint_userinfo": "${CLOUDRON_OIDC_PROFILE_ENDPOINT}",
  "endpoint_token": "${CLOUDRON_OIDC_TOKEN_ENDPOINT}",
  "endpoint_end_session": "",
  "acr_values": "",
  "identity_key": "preferred_username",
  "no_sslverify": "0",
  "http_request_timeout": "5",
  "enforce_privacy": "0",
  "alternate_redirect_uri": "0",
  "nickname_key": "preferred_username",
  "email_format": "{email}",
  "displayname_format": "{given_name} {family_name}",
  "identify_with_username": "1",
  "state_time_limit": "180",
  "token_refresh_enable": "0",
  "link_existing_users": "1",
  "create_if_does_not_exist": "1",
  "redirect_user_back": "1",
  "redirect_on_logout": "1",
  "default_role": "${default_role:-editor}",
  "enable_logging": "0",
  "log_limit": "1000"
}
EOF
)
    fi

    echo "==> Configuring OIDC"
    $wp --format=json option update openid_connect_generic_settings "${oidcConfig}"
fi

echo "==> Starting apache"
APACHE_CONFDIR="" source /etc/apache2/envvars
rm -f "${APACHE_PID_FILE}"
exec /usr/sbin/apache2 -DFOREGROUND

