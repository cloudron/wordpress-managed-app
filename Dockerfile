FROM cloudron/base:4.2.0@sha256:46da2fffb36353ef714f97ae8e962bd2c212ca091108d768ba473078319a47f4

# renovate: datasource=github-tags depName=WordPress/WordPress versioning=regex:^(?<major>\d+)\.(?<minor>\d+)\.?(?<patch>\d+)?$
ARG WORDPRESS_VERSION=6.7.2

# renovate: datasource=github-releases depName=wp-cli/wp-cli versioning=semver extractVersion=^v(?<version>.+)$
ARG WPCLI_VERSION=2.11.0

RUN mkdir -p /app/code /app/pkg
WORKDIR /app/code

RUN curl -L http://wordpress.org/wordpress-${WORDPRESS_VERSION}.tar.gz | tar -xz --strip-components 1 -f - && \
    rm readme.html wp-admin/install.php wp-config-sample.php && \
    mv /app/code/wp-content /app/code/wp-content-vanilla && ln -s /app/data/wp-content /app/code/wp-content && \
    ln -s /app/data/htaccess /app/code/.htaccess && \
    ln -sf /app/data/wp-config.php /app/code/wp-config.php

# https://github.com/wp-cli/wp-cli/releases/
RUN curl -L -o /app/pkg/wp https://github.com/wp-cli/wp-cli/releases/download/v${WPCLI_VERSION}/wp-cli-${WPCLI_VERSION}.phar
RUN chmod +x /app/pkg/wp
# this is required for wp rewrite to work
ENV WP_CLI_CONFIG_PATH=/app/pkg/wp-cli.yml
RUN echo "Defaults:www-data env_keep+=WP_CLI_CONFIG_PATH" >> /etc/sudoers
RUN /bin/echo -e "apache_modules:\n  - mod_rewrite" > /app/pkg/wp-cli.yml

# Get the plugins. If you change version below change the activation as well
# renovate: datasource=github-releases depName=oidc-wp/openid-connect-generic versioning=semver
ARG OIDC_VERSION=3.10.0
RUN curl --fail -L -o /app/pkg/openid-connect-generic.zip https://github.com/oidc-wp/openid-connect-generic/archive/refs/tags/${OIDC_VERSION}.zip

# http://plugins.svn.wordpress.org/smtp-mailer/tags/ (does not seems updated anymore)
# https://wordpress.org/plugins/smtp-mailer/
RUN curl --fail -L -o /app/pkg/smtp-mailer.zip https://downloads.wordpress.org/plugin/smtp-mailer.zip

# plugins like duplicator need wp-snapshots folder to be writeable
RUN ln -sf /app/data/wp-snapshots /app/code/wp-snapshots

RUN chown -R www-data:www-data /app/code

# install RPAF module to override HTTPS, SERVER_PORT, HTTP_HOST based on reverse proxy headers
# https://www.digitalocean.com/community/tutorials/how-to-configure-nginx-as-a-web-server-and-reverse-proxy-for-apache-on-one-ubuntu-16-04-server
RUN mkdir /app/code/rpaf && \
    curl -L https://github.com/gnif/mod_rpaf/tarball/669c3d2ba72228134ae5832c8cf908d11ecdd770 | tar -C /app/code/rpaf -xz --strip-components 1 -f -  && \
    cd /app/code/rpaf && \
    make && \
    make install && \
    rm -rf /app/code/rpaf

# configure rpaf
RUN echo "LoadModule rpaf_module /usr/lib/apache2/modules/mod_rpaf.so" > /etc/apache2/mods-available/rpaf.load && a2enmod rpaf

# ioncube. the extension dir comes from php -i | grep extension_dir
# extension has to appear first, otherwise will error with "The Loader must appear as the first entry in the php.ini file"
# ioncube does not seem to have support for PHP 8.0 (https://blog.ioncube.com/2022/08/12/ioncube-php-8-1-support-faq-were-almost-ready/)
RUN mkdir /tmp/ioncube && \
    curl http://downloads.ioncube.com/loader_downloads/ioncube_loaders_lin_x86-64.tar.gz | tar zxvf - -C /tmp/ioncube && \
    cp /tmp/ioncube/ioncube/ioncube_loader_lin_8.1.so /usr/lib/php/20210902/ && \
    rm -rf /tmp/ioncube && \
    echo "zend_extension=/usr/lib/php/20210902/ioncube_loader_lin_8.1.so" > /etc/php/8.1/apache2/conf.d/00-ioncube.ini && \
    echo "zend_extension=/usr/lib/php/20210902/ioncube_loader_lin_8.1.so" > /etc/php/8.1/cli/conf.d/00-ioncube.ini

# configure apache
# keep the prefork linking below a2enmod since it removes dangling mods-enabled (!)
RUN a2disconf other-vhosts-access-log && \
    echo "Listen 80" > /etc/apache2/ports.conf && \
    a2enmod rewrite headers rewrite expires cache php8.1 ext_filter && \
    rm /etc/apache2/sites-enabled/* && \
    sed -e 's,^ErrorLog.*,ErrorLog "|/bin/cat",' -i /etc/apache2/apache2.conf && \
    ln -sf /app/data/apache/mpm_prefork.conf /etc/apache2/mods-enabled/mpm_prefork.conf

ADD apache/wordpress.conf /etc/apache2/sites-enabled/wordpress.conf

# configure mod_php. apache2ctl -M can be used to list enabled modules
RUN a2enmod rewrite expires headers cache php8.1
RUN crudini --set /etc/php/8.1/apache2/php.ini PHP upload_max_filesize 500M && \
    crudini --set /etc/php/8.1/apache2/php.ini PHP post_max_size 500M && \
    crudini --set /etc/php/8.1/apache2/php.ini PHP max_input_vars 1800 && \
    crudini --set /etc/php/8.1/apache2/php.ini opcache opcache.enable 1 && \
    crudini --set /etc/php/8.1/apache2/php.ini opcache opcache.enable_cli 1 && \
    crudini --set /etc/php/8.1/apache2/php.ini opcache opcache.interned_strings_buffer 8 && \
    crudini --set /etc/php/8.1/apache2/php.ini opcache opcache.max_accelerated_files 10000 && \
    crudini --set /etc/php/8.1/apache2/php.ini opcache opcache.memory_consumption 128 && \
    crudini --set /etc/php/8.1/apache2/php.ini opcache opcache.save_comments 1 && \
    crudini --set /etc/php/8.1/apache2/php.ini opcache opcache.revalidate_freq 1 && \
    crudini --set /etc/php/8.1/apache2/php.ini Session session.save_path /run/wordpress/sessions && \
    crudini --set /etc/php/8.1/apache2/php.ini Session session.gc_probability 1 && \
    crudini --set /etc/php/8.1/apache2/php.ini Session session.gc_divisor 100

RUN cp /etc/php/8.1/apache2/php.ini /etc/php/8.1/cli/php.ini

RUN ln -s /app/data/php.ini /etc/php/8.1/apache2/conf.d/99-cloudron.ini && \
    ln -s /app/data/php.ini /etc/php/8.1/cli/conf.d/99-cloudron.ini

# Allow image magic to generate PDF previews
RUN sed -e 's,.*pattern="PDF".*,,' -i /etc/ImageMagick-6/policy.xml

# Hack to stop sendmail/postfix to keep retrying sending emails because of rofs
RUN rm -rf /var/spool

COPY apache/mpm_prefork.conf start.sh cron.sh /app/pkg/

WORKDIR /tmp
RUN mkdir -p /tmp/cloudron-sso
COPY cloudron-sso.php /tmp/cloudron-sso
RUN zip -r cloudron-sso.zip cloudron-sso && \
    rm -rf /tmp/cloudron-sso && \
    mv /tmp/cloudron-sso.zip /app/pkg/

WORKDIR /app/code

# lock www-data but allow su - www-data to work
RUN passwd -l www-data && usermod --shell /bin/bash --home /app/code www-data

RUN printf '#!/bin/bash\n\nsudo -u www-data -i -- /app/pkg/wp --path=/app/code/ "$@"\n' > /usr/bin/wp && chmod +x /usr/bin/wp

CMD [ "/app/pkg/start.sh" ]
