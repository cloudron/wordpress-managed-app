WordPress (Managed) package brings the user experience of managed WordPress hosting
to your server. With the Managed edition, WordPress releases are tracked by the Cloudron team
and your WordPress installation can be updated from the Cloudron dashboard (like other apps).

To make seamless updates possible, this package has some limitations:

* WordPress core files are read-only. WordPress can still be extended using plugins. If you require
  the flexibility to edit files, use the WordPress (Developer) package instead.
* WordPress cannot be updated from WordPress' admin dashboard (because the code is read-only). Instead,
  you have to update WordPress using Cloudron's dashboard.
* While we have tested extensively with many plugins, some plugins may not work because of the read-only
  setup of WordPress. For example, many migration plugins overwrite WordPress code files during restore
  and are known to not work. We recommend using WordPress (Developer) package for importing your existing site.

## About

WordPress is web software you can use to create a beautiful website or blog.
We like to say that WordPress is both free and priceless at the same time.

The core software is built by hundreds of community volunteers, and when
you’re ready for more there are thousands of plugins and themes available
to transform your site into almost anything you can imagine. Over 60 million
people have chosen WordPress to power the place on the web they call “home” 
— we’d love you to join the family.

### Apps

* [Android](https://play.google.com/store/apps/details?id=org.wordpress.android&hl=en)
* [iOS](https://itunes.apple.com/us/app/wordpress/id335703880?mt=8&uo=6&at=&ct=)

