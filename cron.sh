#!/bin/bash

set -eu

echo "=> Run cron job"

cd /app/code
exec /usr/local/bin/gosu www-data:www-data /app/pkg/wp cron event run --due-now
